import { fromJS } from 'immutable'

import { 
  SAY_HELLO,
  SAY_HELLO_ASYNC_REQUEST,
  SAY_HELLO_ASYNC_SUCCESS,
  SAY_HELLO_ASYNC_FAILURE
} from '../action/hello'

const initialState = fromJS({
  message: 'Initial reducer message',
  messageAsync: 'Initial reducer message for async call'
})

const handlers = {
  [SAY_HELLO]: (state, action) => ( state.set('message', action.payload) ),
  [SAY_HELLO_ASYNC_REQUEST]: (state, action) => { 
    return state.set('messageAsync', 'Data loading...')
  },
  [SAY_HELLO_ASYNC_SUCCESS]: (state, action) => { 
    return state.set('messageAsync', action.payload)
  },
  [SAY_HELLO_ASYNC_FAILURE]: (state, action) => { 
    return state.set('messageAsync', 'Server error')
  }
}

const reducer = (state = initialState, action) => {
  const handler = handlers[action.type]

  return handler ? handler(state, action) : state
}

export default reducer
