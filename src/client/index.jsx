import  'babel-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { AppContainer  } from 'react-hot-loader'
import { BrowserRouter } from 'react-router-dom'

import { isProd } from '../shared/util'

import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware, compose  } from 'redux'
import thunkMiddleware from 'redux-thunk'

import App from './App'
import { APP_CONTAINER_SELECTOR } from '../shared/config'

import helloReducer from './reducer/hello'

const composeEnhancers = (isProd ? undefined : window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose

const store = createStore(combineReducers({
  hello: helloReducer
}), composeEnhancers(applyMiddleware(thunkMiddleware)))  // for react dev tool on chrome

const ROOT_EL = document.querySelector(APP_CONTAINER_SELECTOR)
const wrapApp = (AppComponent, reduxStore) => (
  <Provider store={reduxStore}>
      <BrowserRouter>
        <AppContainer>
          <AppComponent />
        </AppContainer>
      </BrowserRouter>
  </Provider>
)

render(
  wrapApp(App, store),
  ROOT_EL
)

if (module.hot) {
  module.hot.accept('./App', () => {
    // eslint-disable-next-line global-require
    const NextApp = require('./App').default

    render(wrapApp(NextApp, store), ROOT_EL)
  })
}
